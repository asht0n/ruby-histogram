f = File.new(ARGV[0], "r");
file_arr = f.readlines();
hist = Hash.new(0);
count = 0;

file_arr.each do |prog|
	line_arr =  `objdump -d #{prog}`.split("\n");
	 line_arr.each do |line|
		  word_arr = line.split("\t")
		  if word_arr[2] != nil then 
				myword = word_arr[2].split(" ")[0]
				hist[myword] += 1;
				count += 1;
		  end
	 end
end

sorted_hist = hist.sort_by {|_key, value| value}
sorted_hist.each do |el|
	 puts el.inspect;
end
puts "total commands #{count}"
